package org.opencv.android;

import android.util.Log;

import org.opencv.core.Core;

import java.util.StringTokenizer;

class StaticHelper {

    private static final String TAG = "OpenCV/StaticHelper";

    public static boolean initOpenCV(boolean InitCuda)
    {
        boolean result;
        String libs = "";

        if(InitCuda)
        {
            loadLibrary("cudart");
            loadLibrary("nppc");
            loadLibrary("nppi");
            loadLibrary("npps");
            loadLibrary("cufft");
            loadLibrary("cublas");
        }


        try
        {
            System.loadLibrary("opencv_info");
            libs = getLibraryList();
        }
        catch(UnsatisfiedLinkError e)
        {
            Log.e(TAG, "OpenCV error: Cannot load info library for OpenCV");
        }

        if (initOpenCVLibs(libs))
        {
            String eol = System.getProperty("line.separator");
            for (String str : Core.getBuildInformation().split(eol))
                Log.i(TAG, str);

            result = true;
        }
        else
        {
            result = false;
        }

        return result;
    }

    private static boolean loadLibrary(String Name)
    {
        boolean result = true;
        try
        {
            System.loadLibrary(Name);
        }
        catch(UnsatisfiedLinkError e)
        {
            e.printStackTrace();
            result = false;
        }

        return result;
    }

    private static boolean initOpenCVLibs(String Libs)
    {
        boolean result = true;

        if ((null != Libs) && (Libs.length() != 0))
        {
            StringTokenizer splitter = new StringTokenizer(Libs, ";");
            while(splitter.hasMoreTokens())
            {
                result &= loadLibrary(splitter.nextToken());
            }
        }
        else
        {
            // If dependencies list is not defined or empty.
            result = loadLibrary("opencv_java4");
        }

        return result;
    }

    private static native String getLibraryList();
}
