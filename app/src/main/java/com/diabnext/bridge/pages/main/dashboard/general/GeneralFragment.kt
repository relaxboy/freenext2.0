package com.diabnext.bridge.pages.main.dashboard.general

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.diabnext.bridge.R
import com.diabnext.bridge.models.datas.DiabRecordToCreate
import com.diabnext.bridge.screen_shot.GraphExtractor
import com.diabnext.bridge.screen_shot.GraphExtractor.InitCallback
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class GeneralFragment : Fragment() {
    private val TAG = this.javaClass.name
    private var isExtratingLibreGraph = false
    private val mSdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault())
//    private var context = null
    private var textView_date: TextView? = null
    private var recyclerView: RecyclerView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        this.context = inflater.context
        return inflater.inflate(R.layout.fragment_dashboard_general, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        var scanButton:Button = view.findViewById(R.id.scanButton)
        textView_date = view.findViewById(R.id.textView_date)
        recyclerView = view.findViewById(R.id.recyclerView)
        scanButton.setOnClickListener(View.OnClickListener {
            //                refreshGraph();
            val calendar = Calendar.getInstance()
            //Get last picture of date.
            startScan(calendar.timeInMillis)
            
            //calendar.add(Calendar.MONTH, -1)
            //calendar.add(Calendar.DAY_OF_YEAR, -11)
            //Get last picture, then set record time to [date].
            //startScan2(calendar.getTimeInMillis());
        })
    }

    override fun onResume() {
        super.onResume()
        //        refreshGraph();
    }

    override fun onPause() {
        super.onPause()
        //        refreshGraph();
    }

    private fun startScan(date: Long) { //Get last picture of date.
        GraphExtractor.instance.init(context!!, object : InitCallback {
            //                                                Log.d(TAG, new Date(data.getMinutesInDay() * 60000).toString());
            override val isReady: Unit
                get() {
                    Log.d(TAG, "startScan")
                    if (!isExtratingLibreGraph && ContextCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        isExtratingLibreGraph = true
                        object : Thread() {
                            override fun run() {
                                val calendar = Calendar.getInstance()
                                calendar.timeInMillis = date
                                calendar[Calendar.HOUR_OF_DAY] = 0
                                calendar[Calendar.MINUTE] = 0
                                calendar[Calendar.SECOND] = 0
                                calendar[Calendar.MILLISECOND] = 0
                                val date1 = calendar.timeInMillis
                                calendar.add(Calendar.DAY_OF_YEAR, +1)
                                val date2 = calendar.timeInMillis
                                val newRecords =
                                    ArrayList<DiabRecordToCreate>()
                                val cursor: Cursor
                                val column_index_data: Int
                                val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                val projection = arrayOf(
                                    MediaStore.MediaColumns.DATA,
                                    MediaStore.Images.Media.DATE_TAKEN
                                )
                                cursor = activity!!.contentResolver.query(
                                    uri,
                                    projection,
                                    MediaStore.Images.Media.DATE_TAKEN + " >= " + date1 + " and " + MediaStore.Images.Media.DATE_TAKEN + " < " + date2,
                                    null,
                                    MediaStore.Images.Media.DATE_TAKEN + " ASC"
                                )
                                column_index_data =
                                    cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                                val op = BitmapFactory.Options()
                                op.inJustDecodeBounds = true
                                val displayMetrics = DisplayMetrics()
                                activity!!.windowManager.defaultDisplay
                                    .getRealMetrics(displayMetrics)
                                val phoneHeight = displayMetrics.heightPixels
                                val phoneWidth = displayMetrics.widthPixels
                                if (cursor.moveToLast()) {
                                    var bmp: Bitmap? = null
                                    try {
                                        val path = cursor.getString(column_index_data)
                                        Log.d(
                                            TAG,
                                            "column_index_data = $column_index_data,path = $path"
                                        )
                                        bmp = BitmapFactory.decodeFile(path, op)
                                        if ((op.outWidth != phoneWidth || op.outHeight != phoneHeight) && (op.outWidth != phoneHeight || op.outHeight != phoneWidth)) {
                                        } else {
                                            Log.d(TAG,"------>1")
                                            bmp = BitmapFactory.decodeFile(path)
                                            val newData = GraphExtractor.instance.getGraphData(bmp)
                                            if(newData == null) Log.d(TAG,"------>2")
                                            else if(newData.isEmpty()) Log.d(TAG,"------>3")
                                            if (newData == null || newData.isEmpty()) throw Exception(
                                                "NO DATA"
                                            )
                                            val calOfDay =
                                                Calendar.getInstance()
                                            calOfDay.timeInMillis = date
                                            if (newData[newData.size - 1].minutesInDay >= 60 * 24) calOfDay.add(
                                                Calendar.DAY_OF_YEAR,
                                                -1
                                            )
                                            calOfDay[Calendar.HOUR_OF_DAY] = 0
                                            calOfDay[Calendar.MINUTE] = 0
                                            calOfDay[Calendar.SECOND] = 0
                                            calOfDay[Calendar.MILLISECOND] = 0
                                            val file = File(path)
                                            textView_date!!.text = mSdf.format(file.lastModified())
                                            var lastDataTime: Long = 0
                                            for (data in newData) if (calOfDay.timeInMillis + data.minutesInDay * 60000 > lastDataTime) {
                                                lastDataTime =
                                                    calOfDay.timeInMillis + data.minutesInDay * 60000
                                                //                                                Log.d(TAG, new Date(data.getMinutesInDay() * 60000).toString());
                                                Log.d(TAG, "value = " + data.value)
                                                val diabRecord =
                                                    DiabRecordToCreate()
                                                diabRecord.date = Date(lastDataTime)
                                                diabRecord.glycemie = data.value.toFloat()
                                                diabRecord.glucoseProductID = 85
                                                newRecords.add(diabRecord)
                                            }
                                        }
                                    } catch (e: Exception) {
                                    } finally {
                                        if (bmp != null && !bmp.isRecycled) bmp.recycle()
                                    }
                                } else Toast.makeText(
                                    context,
                                    "No picture.",
                                    Toast.LENGTH_LONG
                                ).show()
                                Log.d(TAG, "SIZE new freestyle :  " + newRecords.size)
                                if (newRecords.size == 0) {
                                    isExtratingLibreGraph = false
                                } else {
                                    isExtratingLibreGraph = false
                                    val linearLayoutManager =
                                        LinearLayoutManager(context)
                                    linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
                                    recyclerView!!.layoutManager = LinearLayoutManager(context)
                                    val myRecyclerViewAdapter =
                                        MyRecyclerViewAdapter(newRecords)
                                    recyclerView!!.adapter = myRecyclerViewAdapter
                                }
                            }
                        }.run()
                    }
                }
        })
    }

    private fun startScan2(date: Long) { //Get last picture, then set record time to [date].
        GraphExtractor.instance.init(context!!, object : InitCallback {
            //                                                Log.d(TAG, new Date(data.getMinutesInDay() * 60000).toString());
            override val isReady: Unit
                get() {
                    Log.d(TAG, "startScan")
                    if (!isExtratingLibreGraph && ContextCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        isExtratingLibreGraph = true
                        object : Thread() {
                            override fun run() {
                                val newRecords =
                                    ArrayList<DiabRecordToCreate>()
                                var lastDataTime =
                                    System.currentTimeMillis() - 604800000
                                val cursor: Cursor
                                val column_index_data: Int
                                val column_index_date_taken: Int
                                val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                val projection = arrayOf(
                                    MediaStore.MediaColumns.DATA,
                                    MediaStore.Images.Media.DATE_TAKEN
                                )
                                cursor = activity!!.contentResolver.query(
                                    uri,
                                    projection,
                                    MediaStore.Images.Media.DATE_TAKEN + " > " + lastDataTime,
                                    null,
                                    MediaStore.Images.Media.DATE_TAKEN + " ASC"
                                )
                                column_index_data =
                                    cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                                column_index_date_taken =
                                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN)
                                val op = BitmapFactory.Options()
                                op.inJustDecodeBounds = true
                                val displayMetrics = DisplayMetrics()
                                activity!!.windowManager.defaultDisplay
                                    .getRealMetrics(displayMetrics)
                                val phoneHeight = displayMetrics.heightPixels
                                val phoneWidth = displayMetrics.widthPixels
                                if (cursor.moveToLast()) {
                                    var bmp: Bitmap? = null
                                    try {
                                        val path = cursor.getString(column_index_data)
                                        Log.d(
                                            TAG,
                                            "column_index_data = $column_index_data,path = $path"
                                        )
                                        bmp = BitmapFactory.decodeFile(path, op)
                                        if ((op.outWidth != phoneWidth || op.outHeight != phoneHeight) && (op.outWidth != phoneHeight || op.outHeight != phoneWidth)) {
                                        } else {
                                            bmp = BitmapFactory.decodeFile(path)
                                            val newData =
                                                GraphExtractor.instance.getGraphData(bmp)
                                            if (newData == null || newData.isEmpty()) throw Exception(
                                                "NO DATA"
                                            )
                                            val calOfDay =
                                                Calendar.getInstance()
                                            calOfDay.timeInMillis = date
                                            if (newData[newData.size - 1].minutesInDay >= 60 * 24) calOfDay.add(
                                                Calendar.DAY_OF_YEAR,
                                                -1
                                            )
                                            calOfDay[Calendar.HOUR_OF_DAY] = 0
                                            calOfDay[Calendar.MINUTE] = 0
                                            calOfDay[Calendar.SECOND] = 0
                                            calOfDay[Calendar.MILLISECOND] = 0
                                            val file = File(path)
                                            val calendar =
                                                Calendar.getInstance()
                                            calendar.timeInMillis = file.lastModified()
                                            calendar[Calendar.YEAR] = calOfDay[Calendar.YEAR]
                                            calendar[Calendar.MONTH] = calOfDay[Calendar.MONTH]
                                            calendar[Calendar.DAY_OF_MONTH] =
                                                calOfDay[Calendar.DAY_OF_MONTH]
                                            textView_date!!.text = mSdf.format(calendar.time)
                                            lastDataTime = 0
                                            for (data in newData) if (calOfDay.timeInMillis + data.minutesInDay * 60000 > lastDataTime) {
                                                lastDataTime =
                                                    calOfDay.timeInMillis + data.minutesInDay * 60000
                                                //                                                Log.d(TAG, new Date(data.getMinutesInDay() * 60000).toString());
                                                Log.d(TAG, "value = " + data.value)
                                                val diabRecord =
                                                    DiabRecordToCreate()
                                                diabRecord.date = Date(lastDataTime)
                                                diabRecord.glycemie = data.value.toFloat()
                                                diabRecord.glucoseProductID = 85
                                                newRecords.add(diabRecord)
                                            }
                                        }
                                    } catch (e: Exception) {
                                    } finally {
                                        if (bmp != null && !bmp.isRecycled) bmp.recycle()
                                    }
                                }
                                Log.d(TAG, "SIZE new freestyle :  " + newRecords.size)
                                if (newRecords.size == 0) {
                                    isExtratingLibreGraph = false
                                } else {
                                    isExtratingLibreGraph = false
                                    val linearLayoutManager =
                                        LinearLayoutManager(context)
                                    linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
                                    recyclerView!!.layoutManager = LinearLayoutManager(context)
                                    val myRecyclerViewAdapter =
                                        MyRecyclerViewAdapter(newRecords)
                                    recyclerView!!.adapter = myRecyclerViewAdapter
                                }
                            }
                        }.run()
                    }
                }
        })
    }

    private fun refreshGraph() {
        GraphExtractor.instance.init(context!!, object : InitCallback {
            //                                                Log.d(TAG, new Date(data.getMinutesInDay() * 60000).toString());
            //                            while (cursor.moveToNext()) {
//
//                            }
            //                            long lastDataTime = System.currentTimeMillis() - 25200000;
            override val isReady: Unit
                get() {
                    Log.d(TAG, "isReady")
                    if (!isExtratingLibreGraph && ContextCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        isExtratingLibreGraph = true
                        object : Thread() {
                            override fun run() {
                                val calOfDay = Calendar.getInstance()
                                val newRecords =
                                    ArrayList<DiabRecordToCreate>()
                                var lastDataTime =
                                    System.currentTimeMillis() - 604800000
                                //                            long lastDataTime = System.currentTimeMillis() - 25200000;
                                Log.d(
                                    TAG,
                                    "GeneralFragment -> LAST DATE TIME = $lastDataTime"
                                )
                                Log.d(
                                    TAG,
                                    "GeneralFragment -> " + "LAST DATE = " + Date(lastDataTime)
                                )
                                val cursor: Cursor
                                val column_index_data: Int
                                val column_index_date_taken: Int
                                val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                val projection = arrayOf(
                                    MediaStore.MediaColumns.DATA,
                                    MediaStore.Images.Media.DATE_TAKEN
                                )
                                cursor = activity!!.contentResolver.query(
                                    uri,
                                    projection,
                                    MediaStore.Images.Media.DATE_TAKEN + " > " + lastDataTime,
                                    null,
                                    MediaStore.Images.Media.DATE_TAKEN + " ASC"
                                )
                                column_index_data =
                                    cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                                column_index_date_taken =
                                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN)
                                val op = BitmapFactory.Options()
                                op.inJustDecodeBounds = true
                                val displayMetrics = DisplayMetrics()
                                activity!!.windowManager.defaultDisplay
                                    .getRealMetrics(displayMetrics)
                                val phoneHeight = displayMetrics.heightPixels
                                val phoneWidth = displayMetrics.widthPixels
                                if (cursor.moveToLast()) {
                                    var bmp: Bitmap? = null
                                    try {
                                        val path = cursor.getString(column_index_data)
                                        Log.d(
                                            TAG,
                                            "column_index_data = $column_index_data,path = $path"
                                        )
                                        val file = File(path)
                                        textView_date!!.text =
                                            mSdf.format(Date(file.lastModified()))
                                        bmp = BitmapFactory.decodeFile(path, op)
                                        if ((op.outWidth != phoneWidth || op.outHeight != phoneHeight) && (op.outWidth != phoneHeight || op.outHeight != phoneWidth)) {
                                        } else {
                                            bmp = BitmapFactory.decodeFile(path)
                                            val newData =
                                                GraphExtractor.instance.getGraphData(bmp)
                                            if (newData == null || newData.isEmpty()) throw Exception(
                                                "NO DATA"
                                            )
                                            calOfDay.timeInMillis = cursor.getLong(
                                                column_index_date_taken
                                            )
                                            if (newData[newData.size - 1].minutesInDay >= 60 * 24) calOfDay.add(
                                                Calendar.DAY_OF_YEAR,
                                                -1
                                            )
                                            calOfDay[Calendar.HOUR_OF_DAY] = 0
                                            calOfDay[Calendar.MINUTE] = 0
                                            calOfDay[Calendar.SECOND] = 0
                                            calOfDay[Calendar.MILLISECOND] = 0
                                            for (data in newData) if (calOfDay.timeInMillis + data.minutesInDay * 60000 > lastDataTime) {
                                                lastDataTime =
                                                    calOfDay.timeInMillis + data.minutesInDay * 60000
                                                //                                                Log.d(TAG, new Date(data.getMinutesInDay() * 60000).toString());
                                                Log.d(TAG, "value = " + data.value)
                                                val diabRecord =
                                                    DiabRecordToCreate()
                                                diabRecord.date = Date(lastDataTime)
                                                diabRecord.glycemie = data.value.toFloat()
                                                diabRecord.glucoseProductID = 85
                                                newRecords.add(diabRecord)
                                            }
                                        }
                                    } catch (e: Exception) {
                                    } finally {
                                        if (bmp != null && !bmp.isRecycled) bmp.recycle()
                                    }
                                }
                                //                            while (cursor.moveToNext()) {
//
//                            }
                                Log.d(TAG, "SIZE new freestyle :  " + newRecords.size)
                                if (newRecords.size == 0) {
                                    isExtratingLibreGraph = false
                                } else {
                                    isExtratingLibreGraph = false
                                    val linearLayoutManager =
                                        LinearLayoutManager(context)
                                    linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
                                    recyclerView!!.layoutManager = LinearLayoutManager(context)
                                    val myRecyclerViewAdapter =
                                        MyRecyclerViewAdapter(newRecords)
                                    recyclerView!!.adapter = myRecyclerViewAdapter
                                }
                            }
                        }.run()
                    }
                }
        })
    }

    private inner class MyRecyclerViewAdapter(private val newRecords: ArrayList<DiabRecordToCreate>) :
        RecyclerView.Adapter<MyViewHolder>() {
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder { // create a new view
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.record_holder, parent, false)
            return MyViewHolder(v)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val diabRecordToCreate = newRecords[position]
            holder.textView_date.text = mSdf.format(diabRecordToCreate.date)
            holder.textView_value.text = diabRecordToCreate.glycemie.toString()
            holder.bgView.setBackgroundColor(
                if (position % 2 == 0) ContextCompat.getColor(
                    context!!,
                    R.color.grayLight
                ) else Color.WHITE
            )
        }

        override fun getItemCount(): Int {
            return newRecords.size
        }

    }

    class MyViewHolder(v: View) : ViewHolder(v) {
        // each data item is just a string in this case
        var textView_value: TextView
        var textView_date: TextView
        var bgView: LinearLayout

        init {
            textView_value = v.findViewById(R.id.textView_value)
            textView_date = v.findViewById(R.id.textView_date)
            bgView = v.findViewById(R.id.bgView)
        }
    }

    companion object {
        fun newInstance(): GeneralFragment {
            return GeneralFragment()
        }
    }
}