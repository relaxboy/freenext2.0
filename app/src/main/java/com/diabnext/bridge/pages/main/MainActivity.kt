package com.diabnext.bridge.pages.main

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.diabnext.bridge.R
import com.diabnext.bridge.pages.main.dashboard.general.GeneralFragment

class MainActivity : FragmentActivity() {
    private var generalFragment: GeneralFragment? = null
    private val TAG = this.javaClass.name
    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        generalFragment = GeneralFragment.newInstance()
        val transaction =
            supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentsContainer, generalFragment!!, "generalFragment").commit()
    }

    override fun onResume() {
        super.onResume()
    }
}