package com.diabnext.bridge.models.datas

import java.io.Serializable
import java.util.*

class DiabRecordToCreate : Serializable {
    var id = 0
    var date: Date? = null
    var glycemie: Float? = null
    var fastInsulin: Float? = null
    var slowInsuline: Float? = null
    var mealDescription: String? = null
    var carbs: Int? = null
    var activitySteps: Int? = null
    var activityMinutes: Int? = null
    var pictures: ArrayList<String>? = null
    var serialNumber: String? = null
    var fastInsulinProductID: Int? = null
    var slowInsulinProductID: Int? = null
    var glucoseProductID: Int? = null
    var productPumpId: Int? = null

    val isEmpty: Boolean
        get() = if (date == null) true else glycemie == null && fastInsulin == null && slowInsuline == null && carbs == null && activitySteps == null && activityMinutes == null

    override fun toString(): String {
        return "DiabRecordToCreate{" +
                "date" + date.toString() +
                "fastInsulin=" + fastInsulin +
                ", slowInsuline=" + slowInsuline +
                ", productPumpId=" + productPumpId +
                '}'
    }
}