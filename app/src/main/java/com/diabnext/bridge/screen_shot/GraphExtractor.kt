package com.diabnext.bridge.screen_shot

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.diabnext.bridge.R
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import java.util.*

class GraphExtractor private constructor() {
    private var digitsTemplate: ArrayList<Mat>? = null
    private var isInitialized = false
    private var isInitializing = false
    private val sizeDigitTemplate = Size(30.0, 45.0)
    private val TAG = this.javaClass.name
    private val initCallbacks =
        ArrayList<InitCallback>()

    fun init(context: Context, callback: InitCallback?) {
        if (isInitialized) {
            callback?.isReady
            return
        }
        if (callback != null) initCallbacks.add(callback)
        if (isInitializing) return
        isInitializing = true
        val mLoaderCallback: BaseLoaderCallback = object : BaseLoaderCallback(context) {
            override fun onManagerConnected(status: Int) {
                if (status == LoaderCallbackInterface.SUCCESS) {
                    object : Thread() {
                        override fun run() {
                            val mat = Mat()
                            Utils.bitmapToMat(
                                BitmapFactory.decodeResource(
                                    context.resources,
                                    R.drawable.template
                                ), mat
                            )
                            Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGBA2RGB)
                            val range = Mat()
                            Core.inRange(mat, Scalar(0.0, 0.0, 0.0), Scalar(120.0, 120.0, 120.0), range)
                            val numbersContours: List<MatOfPoint> =
                                ArrayList()
                            Imgproc.findContours(
                                range,
                                numbersContours,
                                Mat(),
                                Imgproc.RETR_EXTERNAL,
                                Imgproc.CHAIN_APPROX_SIMPLE
                            )
                            Collections.sort(
                                numbersContours
                            ) { m1, m2 ->
                                (Imgproc.boundingRect(m1).tl().x - Imgproc.boundingRect(
                                    m2
                                ).tl().x).toInt()
                            }
                            digitsTemplate = ArrayList()
                            for (mop in numbersContours) {
                                val roi = Mat(range, Imgproc.boundingRect(mop))
                                Imgproc.resize(roi, roi, sizeDigitTemplate)
                                digitsTemplate!!.add(roi)
                            }
                            isInitialized = true
                            Handler(Looper.getMainLooper())
                                .post {
                                    for (ic in initCallbacks) ic?.isReady
                                    initCallbacks.clear()
                                }
                        }
                    }.start()
                } else super.onManagerConnected(status)
            }
        }
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, context, mLoaderCallback)
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    fun getMinuteOfTheDay(mat: Mat?): Int {
        val range = Mat()
        val contours: List<MatOfPoint> = ArrayList()
        Core.inRange(mat, Scalar(0.0, 0.0, 0.0), Scalar(200.0, 200.0, 200.0), range)
        Imgproc.findContours(
            range,
            contours,
            Mat(),
            Imgproc.RETR_EXTERNAL,
            Imgproc.CHAIN_APPROX_SIMPLE
        )
        Collections.sort(contours) { m1, m2 ->
            (Imgproc.boundingRect(m1).tl().x - Imgproc.boundingRect(
                m2
            ).tl().x).toInt()
        }
        val digitsDetected = ArrayList<Mat>()
        for (i in contours.indices) {
            val matz = Mat(range, Imgproc.boundingRect(contours[i]))
            Imgproc.resize(matz, matz, sizeDigitTemplate)
            digitsDetected.add(matz)
        }
        val digits = ArrayList<Int>()
        for (m1 in digitsDetected) {
            var nbr = -1
            var score = 1000000.0
            for (i in digitsTemplate!!.indices) {
                val m2 = digitsTemplate!![i]
                val result = Mat()
                Imgproc.matchTemplate(m1, m2, result, 1)
                val minMaxLocResult = Core.minMaxLoc(result)
                if (minMaxLocResult.minVal < score) {
                    score = minMaxLocResult.minVal
                    nbr = i
                }
            }
            digits.add(nbr)
        }
        return if (digits.size != 6) -1 else digits[0] * 60 * 10 + digits[1] * 60 + digits[4] * 10 + digits[5]
    }

    fun getGraphData(image: Bitmap?): ArrayList<GraphData>? {
        try {
            var matSrc = Mat()
            Utils.bitmapToMat(image, matSrc)
            Imgproc.resize(
                matSrc,
                matSrc,
                Size(750.0, (matSrc.height() * 750 / matSrc.width()).toDouble())
            )
            Imgproc.cvtColor(matSrc, matSrc, Imgproc.COLOR_BGRA2GRAY)
            val treeshold = Mat()
            Core.inRange(matSrc, Scalar(240.0), Scalar(255.0), treeshold)
            Imgproc.morphologyEx(
                treeshold,
                treeshold,
                Imgproc.MORPH_ERODE,
                Mat(Size(50.0, 50.0), CvType.CV_8UC1, Scalar(255.0))
            )
            val graphArea = Imgproc.boundingRect(treeshold)
            if (graphArea.width < 70) return null
            matSrc = Mat(matSrc, graphArea)
            Core.inRange(matSrc, Scalar(0.0), Scalar(135.0), treeshold)
            var rectCircle: Rect? = null
            val circles = Mat()
            Imgproc.HoughCircles(
                Mat(
                    treeshold,
                    Rect(treeshold.width() - 70, 0, 70, treeshold.height())
                ), circles, Imgproc.HOUGH_GRADIENT, 0.5, 10000.0, 100.0, 7.0, 15, 20
            )
            if (circles.rows() > 0) {
                val circleCoordinates = circles[0, 0]
                rectCircle = Rect(
                    circleCoordinates[0].toInt() - circleCoordinates[2].toInt() + treeshold.width() - 70,
                    circleCoordinates[1].toInt() - circleCoordinates[2].toInt(),
                    circleCoordinates[2].toInt() * 2,
                    circleCoordinates[2].toInt() * 2
                )
                Imgproc.rectangle(treeshold, rectCircle, Scalar(0.0), -1)
            }
            val graph = Mat()
            Imgproc.morphologyEx(
                treeshold,
                graph,
                Imgproc.MORPH_ERODE,
                Mat(Size(4.0, 4.0), CvType.CV_8UC1, Scalar(255.0))
            )
            Imgproc.morphologyEx(
                graph,
                graph,
                Imgproc.MORPH_CLOSE,
                Mat(Size(10.0, 10.0), CvType.CV_8UC1, Scalar(255.0))
            )
            Imgproc.morphologyEx(
                treeshold,
                treeshold,
                Imgproc.MORPH_CLOSE,
                Mat(Size(10.0, 10.0), CvType.CV_8UC1, Scalar(255.0))
            )
            Imgproc.morphologyEx(
                treeshold,
                treeshold,
                Imgproc.MORPH_DILATE,
                Mat(Size(5.0, 5.0), CvType.CV_8UC1, Scalar(255.0))
            )
            val pts: List<MatOfPoint> = ArrayList()
            Imgproc.findContours(
                treeshold,
                pts,
                Mat(),
                Imgproc.RETR_LIST,
                Imgproc.CHAIN_APPROX_SIMPLE
            )
            val rects =
                ArrayList<Rect>()
            for (pt in pts) {
                val rect = Imgproc.boundingRect(pt)
                if (rect.width > rect.height && rect.width < 70 && rect.width > 20 && rect.height > 15 && rect.height < 25) rects.add(
                    rect
                )
            }
            val ordinates =
                ArrayList<Rect>()
            val abscisses =
                ArrayList<Rect>()
            var minX = 1000000
            var maxY = 0
            var minYAbscisses = Int.MAX_VALUE
            for (rect in rects) {
                if (rect.x + rect.width < minX) minX = rect.x + rect.width
                if (rect.y > maxY) maxY = rect.y
            }
            for (rect in rects) {
                if (Math.abs(rect.x + rect.width - minX) < 20) ordinates.add(rect)
                if (Math.abs(rect.y - maxY) < 20) abscisses.add(rect)
                if (rect.y < minYAbscisses) minYAbscisses = rect.y
            }
            if (ordinates.size > 12 || ordinates.size < 5) return null
            if (abscisses.size > 4 || abscisses.size < 2) return null
            ordinates.sortWith(Comparator { r1, r2 -> r2.y - r1.y })
            abscisses.sortWith(Comparator { r1, r2 -> r1.x - r2.x })
            var YOrigin = ordinates[0].y + ordinates[0].height / 2
            val Y50Step = YOrigin - ordinates[1].y - ordinates[1].height / 2
            YOrigin += Y50Step
            val XOrigin = abscisses[0].x + abscisses[0].width / 2
            val XStep = abscisses[1].x + abscisses[1].width / 2 - XOrigin
            val minutesOrigin = getMinuteOfTheDay(Mat(matSrc, abscisses[0]))
            if (minutesOrigin < 0 || minutesOrigin > 36 * 60) return null
            val pointsValue = IntArray(graph.width())
            val pointsCount = IntArray(graph.width())
            for (i in 0 until graph.width()) {
                pointsValue[i] = 0
                pointsCount[i] = 0
            }
            val curvePts = Mat()
            Core.findNonZero(graph, curvePts)
            for (i in 0 until curvePts.rows()) {
                val p = curvePts.row(i)[0, 0]
                if (p[1] < maxY && p[1] > minYAbscisses && p[0] > minX && p[0] < matSrc.width() - 30 && (rectCircle == null || p[0] < rectCircle.x)
                ) {
                    pointsValue[p[0].toInt()] += p[1].toInt()
//                    (pointsValue[p[0].toInt()] += p[1]).toInt()
                    pointsCount[p[0].toInt()]++
                }
            }
            val datas = ArrayList<GraphData>()
            var lastValue = -1
            var lastX = Int.MIN_VALUE
            var newValue: Int
            var time: Int
            val shouldAddADay: Boolean =
                (((-XOrigin).toFloat() / XStep * 3 * 60 + minutesOrigin).toInt())<0
            Log.d("General", "shouldAddADay = $shouldAddADay")
            val p = Point()
            for (i in 0 until matSrc.width()) if (pointsCount[i] > 0) {
                p.x = i.toDouble()
                p.y = pointsValue[i] / pointsCount[i].toDouble()
                newValue = Math.round((YOrigin - p.y).toFloat() / Y50Step * 50)
                if (newValue > 600 || newValue < 10) return null
                if (newValue != lastValue || i - lastX > 3) {
                    lastValue = newValue
                    lastX = i
                    time =
                        ((p.x - XOrigin).toFloat() / XStep * 3 * 60 + minutesOrigin).toInt()
                    val gd = GraphData()
                    gd.value = newValue
                    gd.minutesInDay = if (shouldAddADay) (time + 1440).toLong() else time.toLong()
                    datas.add(gd)
                }
            }
            /*
            if (rectCircle != null) {
                GraphData gd = new GraphData();
                gd.setValue(Math.round((float) (YOrigin - rectCircle.y - rectCircle.height / 2) / Y50Step * 50));
                time = (int) ((float) (rectCircle.x + rectCircle.width / 2 - XOrigin) / XStep * 3 * 60 + minutesOrigin);
                gd.setMinutesInDay(shouldAddADay ? time + 1440 : time);
                datas.add(gd);
            }
            */return if (datas.size < 15) null else datas
        } catch (e: Exception) {
        }
        return null
    }

    interface InitCallback {
        val isReady: Unit
    }

    inner class GraphData {
        var value = 0
        var minutesInDay: Long = 0

    }

    companion object {
        @JvmStatic
        val instance = GraphExtractor()
    }
}