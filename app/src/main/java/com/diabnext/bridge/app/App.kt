package com.diabnext.bridge.app

import android.app.Application
import android.content.Context

class App : Application() {
    var appContext: Context? = null
        private set

    override fun onCreate() {
        super.onCreate()
        instance = this
        appContext = applicationContext
    }

    companion object {
        var instance: App? = null
            private set
    }
}